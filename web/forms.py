#forms.py

from wtforms import Form, BooleanField, DateTimeField
from wtforms import TextAreaField, TextField
from wtform.validators import required, Length

class EstudiantesForm(Form):
	name = TextField('Nombre', [Length(max=255)], [required()])
	last_name = TextField('Apellido', [Length(max=255)])
	comments = TextAreaField('Comentarios', [Length(max=400)])
	active = BooleanField('Activo')