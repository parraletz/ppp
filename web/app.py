from flask import Flask, render_template
from flask import request, url_for
from flask.ext.sqlalchemy import SQLAlchemy
from web.models import Base

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///estudiantes.db'


db = SQLAlchemy(app)
db.Model = Base

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/estudiantes/')
def estudiantes_lista():
    return "Regresa la lista de estudiantes"

@app.route('/estudiantes/<int:estudiante_id>')
def estudiante_detalle(estudiante_id):
    return 'Detalle de estudiante #{}.'.format(estudiante_id)


@app.route( '/estudiantes/<int:estudiante_id>/edit/', methods=['GET', 'POST'])
def estudiante_editar(estudiante_id):
	return 'Formulario para editar el estudiante #{}.'.format(estudiante_id)


@app.route('/estudiantes/create/', methods=['GET', 'POST'])
def estudiante_crear():
	return 'Formulario para crear un nuevo estudiante'

@app.route('/estudiantes/<int:estudiante_id>/delete/', methods=['DELETE'] )
def appointment_delete(estudiante_id):
	raise NotImplementedError('DELETE')





