#models.py

from datetime import datetime
from sqlalchemy import Boolean, Column
from sqlalchemy import DateTime, Integer, String, Text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Estudiantes(Base):
	__tablename__ = 'estudiantes'

	id = Column(Integer, primary_key=True)
	created = Column(DateTime, default=datetime.now)
	modified = Column(DateTime, default=datetime.now, onupdate=datetime.now)

	name = Column(String(255))
	last_name = Column(String(255))
	comments = Column(Text(300))
	active = Column(Boolean, default=True) 

	def __repr__(self):
		return (u'<{self.__class__.__name__}:{self.id}>'.format(self=self))



if __name__ == '__name__':

	from datetime import timedelta

	from sqlalchemy import create_engine
	from sqlalchemy.orm import sessionmaker


	engine = create_engine('sqlite:///estudiantes.db', echo=True)

	Base.metadata.create_all(engine)
	Session = sessionmaker(bind=engine)
	session = Session()


	now = datetime.now()

	session.add(Estudiate(
		name="Alex",
		last_name = "Parra",
		comments = "Python ninja",
		active = True
		))
	session.commit()
